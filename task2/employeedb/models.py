from django.db import models
from django.core.validators import RegexValidator

# Create your models here.


class UserInfo(models.Model):

	alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')
	GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

	employee_name = models.CharField(max_length=200)
	age = models.IntegerField(null=True)
	email = models.EmailField(max_length=50, unique=True, validators=[alphanumeric])
	city = models.CharField(max_length=200)
	pan_number = models.CharField(max_length=50, unique=True, validators=[alphanumeric])
	gender = models.CharField(max_length=1, choices=GENDER_CHOICES,default="F")