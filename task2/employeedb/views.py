from django.shortcuts import render
from django.http import HttpResponse
# from django.db import IntegrityError
from django.contrib import messages


# Create your views here.
from .models import UserInfo

def addinformation(request):
	try:
		if request.method == 'POST':

			# For checking if all feilds are correctly inserted
			if all([request.POST.get("name"),request.POST.get("age"),request.POST.get("email"),request.POST.get("city"),request.POST.get("pan"),request.POST.get("gender")]):

				# populate database here from "name" tag in html

				data = UserInfo()
				data.employee_name = request.POST.get("name")
				data.age = request.POST.get("age")
				data.email = request.POST.get("email")
				data.city = request.POST.get("city")
				data.pan_number = request.POST.get("pan")
				data.gender = request.POST.get("gender")
				
				data.save()					
				return HttpResponse('User Added successfully !!')

			else:
				
				return render(request,'emplyoeedb/index.html')
		
		else:
			return render(request,'emplyoeedb/index.html')
	except:
		return render(request,'emplyoeedb/index.html')

    


def displayinfo(request):
	# fetching all data from tables
	data = UserInfo.objects.all()
	
	context = {
	'data':data
	}
	print(context)
	return render(request,'emplyoeedb/second.html',context)


def searchemployee(request):
	
	# fetching specific data from user
	if request.POST.get("pan"):
		print(request.POST.get("pan"),"\n\n")
		data = UserInfo.objects.filter(pan_number=request.POST.get("pan"))	
		context = {
		'data':data
		}		
	
		return render(request,'emplyoeedb/second.html',context)
	else:
		return HttpResponse('Error has occurred !!')
