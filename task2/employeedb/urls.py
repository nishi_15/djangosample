from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('', views.addinformation, name='addinformation'),
    path('show_stats', views.displayinfo, name='displayinfo'),
    path('search_empl', views.searchemployee, name='searchemployee'),

    
]